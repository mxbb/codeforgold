//
//  Data.swift
//  News
//
//  Created by Birzhan Kerim on 5/31/20.
//  Copyright © 2020 Birzhan Kerim. All rights reserved.
//
/*
 "news_url": "https://www.youtube.com/watch?v=cm8DD9tNC7E",
 "image_url": "https://cdn.snapi.dev/images/v1/h/t/mark-cuban-discusses-coronavirus-and-what-needs-to-be-done-to-restart-the-economy.jpg",
 "title": "Mark Cuban discusses coronavirus, and what needs to be done to restart the economy",
 "text": "Mark Cuban, Dallas Mavericks owner, joins Yahoo Finance's Julia La Roche for Yahoo's Reset Your Mindset at Work special.",
 "source_name": "Yahoo Finance",
 "date": "Sat, 30 May 2020 18:29:41 -0400",
 "topics": [],
 "sentiment": "Neutral",
 "type": "Video"
 */
import Foundation

struct Articles : Decodable {
    var news_url : String
    var image_url : String
    var title : String
    var text : String
    var source_name : String
    var date : String
    var topics : String
    var sentiment : String
    var type : String
    var tickers : [String]
    
    init(dictionary: Dictionary<String, Any>) {
        news_url = dictionary["news_url"] as? String ?? ""
        image_url = dictionary["image_url"] as? String ?? ""
        title = dictionary["title"] as? String ?? ""
        text = dictionary["text"] as? String ?? ""
        source_name = dictionary["source_name"] as? String ?? ""
        date = dictionary["date"] as? String ?? ""
        topics = dictionary["topics"] as? String ?? ""
        sentiment = dictionary["sentiment"] as? String ?? ""
        type = dictionary["type"] as? String ?? ""
        tickers = [dictionary["tickers"] as? String ?? ""]
    }
}
