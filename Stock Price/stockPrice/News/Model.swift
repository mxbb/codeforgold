//
//  Model.swift
//  News
//
//  Created by Birzhan Kerim on 5/31/20.
//  Copyright © 2020 Birzhan Kerim. All rights reserved.
//
//"https://stocknewsapi.com/api/v1?tickers=FB,AMZN,NFLX&items=50&token=urndd8no3cucp5vqownjnfr5nf8dgorz36huxx1m"
import Foundation

var articles:[Articles] {
    let data = try? Data(contentsOf: urlToData)
       if data == nil {
           return []
       }
       
       let rootDictionaryAny = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
       if rootDictionaryAny == nil {
           return []
       }
       
       let rootDictionary = rootDictionaryAny as? Dictionary<String, Any>
       if rootDictionary == nil {
           return []
       }
       
       if let array = rootDictionary!["data"] as? [Dictionary<String, Any>] {
           var returnArray: [Articles] = []
           
           for dict in array {
               let newArticles = Articles(dictionary: dict)
               returnArray.append(newArticles)
           }
           return returnArray
       }
    return []
}

var urlToData: URL {
    let path = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true) [0]+"/dataNews.json"
    let urlPath = URL(fileURLWithPath: path)
    return urlPath
}

func loadNews(completionHandler: (() -> Void)?){
    var count : Int = 0
    let a : String = "https://stocknewsapi.com/api/v1?tickers="
    let c : String = "&items=50&token=urndd8no3cucp5vqownjnfr5nf8dgorz36huxx1m"
    let mass : [String]  = ["FB", "NFLX", "AMZN", "AAPL"]
    var b : String = ""
    
    for mas in mass {
        count +=  +1
    }
    
    for mas in mass {
        if count > 1 {
             b += mas + ","
            count += -1
        } else {
             b += mas
        }
    }
    
    let newsUrl = a + b + c
    
    let url = URL(string: newsUrl)
    let session = URLSession(configuration: .default)
    let downloadTask = session.downloadTask(with: url!) { (urlFile, responce, error) in
        if urlFile != nil {
            try? FileManager.default.replaceItemAt(urlToData, withItemAt: urlFile!)
            completionHandler?()
        }
    }
    downloadTask.resume()
}
