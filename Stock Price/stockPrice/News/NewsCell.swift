//
//  NewsCell.swift
//  News
//
//  Created by Birzhan Kerim on 5/31/20.
//  Copyright © 2020 Birzhan Kerim. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {
    var article: Articles!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var texTLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tickersLabel: UILabel!
    @IBOutlet weak var imageLabel: UIImageView!
    @IBOutlet weak var sourceNameLabel: UILabel!
    @IBOutlet weak var border: UIView!
    
}
