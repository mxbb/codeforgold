//
//  OpenSafariViewController.swift
//  News
//
//  Created by Birzhan Kerim on 6/4/20.
//  Copyright © 2020 Birzhan Kerim. All rights reserved.
//

import UIKit
import SafariServices


class DetailNewsViewController: UIViewController {
    
    var index: Int = 0
    var article: Articles!
    @IBOutlet weak var imageLabel: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var sourceNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var topicsLabel: UILabel!
    @IBOutlet weak var sentimentLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var tickersLabel: UILabel!
    @IBOutlet weak var label: UILabel!
    
    @IBAction func openSafari(_ sender: Any) {
        if let url = URL(string: article.news_url ?? "") {
            let svc = SFSafariViewController(url: url)
            present(svc, animated: true, completion: nil)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        label.layer.borderWidth = 3
        label.layer.borderColor = UIColor.gray.cgColor
        
        // Do any additional setup after loading the view.
        titleLabel.text = article.title
        textLabel.text = article.text
        sourceNameLabel.text = article.source_name
        dateLabel.text = article.date
        topicsLabel.text = article.topics
        sentimentLabel.text = article.sentiment
        typeLabel.text = article.type
        tickersLabel.text = article.tickers[0]
        
        
        DispatchQueue.main.async {
            if let url = URL(string: self.article.image_url) {
                if let data = try? Data(contentsOf: url) {
                    self.imageLabel.image = UIImage(data: data)
                }
            }
        }
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
