//
//  Quote.swift
//  stockPrice
//
//  Created by Zhandos Yernazarov on 5/29/20.
//  Copyright © 2020 Zhandos Yernazarov. All rights reserved.
//

import Foundation

class Quote{
    var quoteResponse: QuoteResponse
    init(dictionary: NSDictionary) {
        self.quoteResponse = dictionary["quoteResponse"] as! QuoteResponse
    }
}

class QuoteResponse{
    var result: [Result]
    init(dictionary: NSDictionary) {
        var dict = dictionary["result"] as! NSArray
        self.result = Result.modelsFromDictionaryArray(array: dict)
    }
}

class Result{
    var symbol = ""
    var shortName = ""
    var regularMarketPrice = 0.0
    var regularMarketChangePercent = 0.0
    var regularMarketPreviousClose = 0.0
    var priceHint = 0
    var regularMarketTime = 0
    var regularMarketChange = 0.0
    var regularMarketVolume = 0
    init(dictionary: NSDictionary) {
        self.symbol = dictionary["symbol"] as? String ?? "Undefined"
        self.shortName = dictionary["shortName"] as? String ?? "Undefined"
        self.regularMarketPrice = dictionary["regularMarketPrice"] as? Double ?? 0
        self.regularMarketChangePercent = dictionary["regularMarketChangePercent"] as? Double ?? 0
        self.regularMarketPreviousClose = dictionary["regularMarketPreviousClose"] as? Double ?? 0
        self.priceHint = dictionary["priceHint"] as? Int ?? 0
        self.regularMarketTime = dictionary["regularMarketTime"] as? Int ?? 0
        self.regularMarketChange = dictionary["regularMarketChange"] as? Double ?? 0
        self.regularMarketVolume = dictionary["regularMarketVolume"] as? Int ?? 0
    }
    public class func modelsFromDictionaryArray(array: NSArray) -> [Result] {
        var models = [Result]()
        for item in array{
            models.append(Result(dictionary: item as! NSDictionary))
        }
        return models
    }
}
