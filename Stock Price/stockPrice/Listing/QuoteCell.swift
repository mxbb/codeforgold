//
//  TableViewCell.swift
//  stockPrice
//
//  Created by Zhandos Yernazarov on 5/28/20.
//  Copyright © 2020 Zhandos Yernazarov. All rights reserved.
//

import UIKit

class QuoteCell: UITableViewCell {
    
    @IBOutlet weak var symbol: UILabel!
    @IBOutlet weak var shortName: UILabel!
    @IBOutlet weak var regularMarketPrice: UILabel!
    @IBOutlet weak var regularMarketChangePercent: UILabel!
    @IBOutlet weak var regularMarketChange: UILabel!
    
}
