//
//  ListingViewController.swift
//  stockPrice
//
//  Created by Zhandos Yernazarov on 5/27/20.
//  Copyright © 2020 Zhandos Yernazarov. All rights reserved.
//

import UIKit



class ListingViewController: UITableViewController, UISearchBarDelegate{
    // MARK: - Table view data source
    var results: [Result]?
    var searchQuote = [Result]()
    var searching = false
    var sorting = false
    @IBOutlet weak var listingTableView: UITableView!
//    @IBAction func Sort(_ sender: Any) {
//        if self.sorting {
//            results = results!.sorted(by: { $0.regularMarketPrice > $1.regularMarketPrice })
//            listingTableView.reloadData()
//            self.sorting = false
//        }
//        else{
//            results = results!.sorted(by: { $0.regularMarketPrice < $1.regularMarketPrice })
//            listingTableView.reloadData()
//            self.sorting = true
//        }
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        createSearchBar()
//        tableView.tableFooterView = UIView()
        fetchQuotes(quoteCompletionHandler: { result, error in
            if let result = result {
                self.results = result
                self.listingTableView.reloadData()
            }
        })
    }
    func createSearchBar(){
        let searchBari = UISearchBar()
        searchBari.showsCancelButton = false
        searchBari.placeholder = "Search quotes here..."
        searchBari.delegate = self
        self.navigationItem.titleView = searchBari
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchQuote = results!.filter({$0.shortName.lowercased().prefix(searchText.count) == searchText.lowercased()})
        searching = true
        listingTableView.reloadData()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.text = ""
        listingTableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching{
            return searchQuote.count
        }
        else{
            return results?.count ?? 4
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuoteCell", for: indexPath) as! QuoteCell
        
        if searching{
            let quote = searchQuote[indexPath.row]
            cell.symbol?.text = quote.symbol
            cell.shortName?.text = quote.shortName
            cell.regularMarketChange?.text = String(format: "%.2f", quote.regularMarketChange )
            cell.regularMarketPrice?.text = String(format: "%.2f $", quote.regularMarketPrice )
            //        "\(quote?.regularMarketPrice ?? 0)$"
            cell.regularMarketChangePercent?.text = String(format: "%.2f %", quote.regularMarketChangePercent )
            //        "\(quote?.regularMarketChangePercent ?? 0)%"
        } else{
            let quote = results?[indexPath.row]
            cell.symbol?.text = quote?.symbol
            cell.shortName?.text = quote?.shortName
            cell.regularMarketChange?.text = String(format: "%.2f", quote?.regularMarketChange ?? 0)
            cell.regularMarketPrice?.text = String(format: "%.2f $", quote?.regularMarketPrice ?? 0)
            //        "\(quote?.regularMarketPrice ?? 0)$"
            cell.regularMarketChangePercent?.text = String(format: "%.2f %", quote?.regularMarketChangePercent ?? 0)
            //        "\(quote?.regularMarketChangePercent ?? 0)%"
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           performSegue(withIdentifier: "detailListing", sender: self)
       }
       
       
       
       // MARK: - Navigation

       // In a storyboard-based application, you will often want to do a little preparation before navigation
       override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailListing" {
            (segue.destination as? DetailListingViewController)?.results = results?[tableView.indexPathForSelectedRow!.row]
        }
       }
}
