//
//  DetailListingViewController.swift
//  stockPrice
//
//  Created by Birzhan Kerim on 6/8/20.
//  Copyright © 2020 Zhandos Yernazarov. All rights reserved.
//

import UIKit

class DetailListingViewController: UIViewController {
    
    var index = 0
    
    @IBOutlet weak var symbol: UILabel!
    @IBOutlet weak var regularMarketPrice: UILabel!
    @IBOutlet weak var regularMarketChangePercent: UILabel!
    @IBOutlet weak var regularMarketChange: UILabel!
    @IBOutlet weak var regularMarketPreviousClose: UILabel!
    @IBOutlet weak var priceHint: UILabel!
    @IBOutlet weak var regularMarketTime: UILabel!
    @IBOutlet weak var regularMarketVolume: UILabel!

    
    var results: Result!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        symbol.text = results.symbol
        regularMarketPrice.text = String(results.regularMarketPrice)
        regularMarketChange.text = String(results.regularMarketChange)
        regularMarketChangePercent.text = String(format: "%.2f", results.regularMarketChangePercent) + "%"
        regularMarketPreviousClose.text = String(results.regularMarketPreviousClose)
        priceHint.text = String(results.priceHint)
        regularMarketTime.text = String(results.regularMarketTime)
        regularMarketVolume.text = String(results.regularMarketVolume)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
